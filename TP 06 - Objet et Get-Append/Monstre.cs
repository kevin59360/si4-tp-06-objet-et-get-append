﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TP_06___Objet_et_Get_Append
{
    class Monstre
    {
        public int Vie { get; set; }
        public Image Image { get; set; }
    }
}
