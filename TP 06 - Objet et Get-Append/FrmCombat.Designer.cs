﻿namespace TP_06___Objet_et_Get_Append
{
    partial class FrmCombat
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChoisirJ1 = new System.Windows.Forms.Button();
            this.grpJoueur1 = new System.Windows.Forms.GroupBox();
            this.picMonstre1 = new System.Windows.Forms.PictureBox();
            this.progressBarMonstre1 = new System.Windows.Forms.ProgressBar();
            this.btnAttaque1J1 = new System.Windows.Forms.Button();
            this.btnAttaque2J1 = new System.Windows.Forms.Button();
            this.grpJoueur2 = new System.Windows.Forms.GroupBox();
            this.btnAttaque2J2 = new System.Windows.Forms.Button();
            this.btnAttaque1J2 = new System.Windows.Forms.Button();
            this.progressBarMonstre2 = new System.Windows.Forms.ProgressBar();
            this.picMonstre2 = new System.Windows.Forms.PictureBox();
            this.btnChoisirJ2 = new System.Windows.Forms.Button();
            this.grpJoueur1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMonstre1)).BeginInit();
            this.grpJoueur2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMonstre2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnChoisirJ1
            // 
            this.btnChoisirJ1.Location = new System.Drawing.Point(6, 19);
            this.btnChoisirJ1.Name = "btnChoisirJ1";
            this.btnChoisirJ1.Size = new System.Drawing.Size(75, 23);
            this.btnChoisirJ1.TabIndex = 0;
            this.btnChoisirJ1.Text = "Choisir";
            this.btnChoisirJ1.UseVisualStyleBackColor = true;
            this.btnChoisirJ1.Click += new System.EventHandler(this.btnChoisirJ1_Click);
            // 
            // grpJoueur1
            // 
            this.grpJoueur1.Controls.Add(this.btnAttaque2J1);
            this.grpJoueur1.Controls.Add(this.btnAttaque1J1);
            this.grpJoueur1.Controls.Add(this.progressBarMonstre1);
            this.grpJoueur1.Controls.Add(this.picMonstre1);
            this.grpJoueur1.Controls.Add(this.btnChoisirJ1);
            this.grpJoueur1.Location = new System.Drawing.Point(12, 12);
            this.grpJoueur1.Name = "grpJoueur1";
            this.grpJoueur1.Size = new System.Drawing.Size(317, 458);
            this.grpJoueur1.TabIndex = 1;
            this.grpJoueur1.TabStop = false;
            this.grpJoueur1.Text = "Joueur 1";
            // 
            // picMonstre1
            // 
            this.picMonstre1.Location = new System.Drawing.Point(6, 48);
            this.picMonstre1.Name = "picMonstre1";
            this.picMonstre1.Size = new System.Drawing.Size(300, 400);
            this.picMonstre1.TabIndex = 1;
            this.picMonstre1.TabStop = false;
            // 
            // progressBarMonstre1
            // 
            this.progressBarMonstre1.Location = new System.Drawing.Point(6, 48);
            this.progressBarMonstre1.Name = "progressBarMonstre1";
            this.progressBarMonstre1.Size = new System.Drawing.Size(300, 23);
            this.progressBarMonstre1.TabIndex = 2;
            // 
            // btnAttaque1J1
            // 
            this.btnAttaque1J1.Location = new System.Drawing.Point(87, 19);
            this.btnAttaque1J1.Name = "btnAttaque1J1";
            this.btnAttaque1J1.Size = new System.Drawing.Size(75, 23);
            this.btnAttaque1J1.TabIndex = 3;
            this.btnAttaque1J1.Text = "Attaque 1";
            this.btnAttaque1J1.UseVisualStyleBackColor = true;
            this.btnAttaque1J1.Click += new System.EventHandler(this.btnAttaque1J1_Click);
            // 
            // btnAttaque2J1
            // 
            this.btnAttaque2J1.Location = new System.Drawing.Point(168, 19);
            this.btnAttaque2J1.Name = "btnAttaque2J1";
            this.btnAttaque2J1.Size = new System.Drawing.Size(75, 23);
            this.btnAttaque2J1.TabIndex = 4;
            this.btnAttaque2J1.Text = "Attaque 2";
            this.btnAttaque2J1.UseVisualStyleBackColor = true;
            this.btnAttaque2J1.Click += new System.EventHandler(this.btnAttaque2J1_Click);
            // 
            // grpJoueur2
            // 
            this.grpJoueur2.Controls.Add(this.btnAttaque2J2);
            this.grpJoueur2.Controls.Add(this.btnAttaque1J2);
            this.grpJoueur2.Controls.Add(this.progressBarMonstre2);
            this.grpJoueur2.Controls.Add(this.picMonstre2);
            this.grpJoueur2.Controls.Add(this.btnChoisirJ2);
            this.grpJoueur2.Location = new System.Drawing.Point(335, 12);
            this.grpJoueur2.Name = "grpJoueur2";
            this.grpJoueur2.Size = new System.Drawing.Size(317, 458);
            this.grpJoueur2.TabIndex = 5;
            this.grpJoueur2.TabStop = false;
            this.grpJoueur2.Text = "Joueur 2";
            // 
            // btnAttaque2J2
            // 
            this.btnAttaque2J2.Location = new System.Drawing.Point(168, 19);
            this.btnAttaque2J2.Name = "btnAttaque2J2";
            this.btnAttaque2J2.Size = new System.Drawing.Size(75, 23);
            this.btnAttaque2J2.TabIndex = 4;
            this.btnAttaque2J2.Text = "Attaque 2";
            this.btnAttaque2J2.UseVisualStyleBackColor = true;
            this.btnAttaque2J2.Click += new System.EventHandler(this.btnAttaque2J2_Click);
            // 
            // btnAttaque1J2
            // 
            this.btnAttaque1J2.Location = new System.Drawing.Point(87, 19);
            this.btnAttaque1J2.Name = "btnAttaque1J2";
            this.btnAttaque1J2.Size = new System.Drawing.Size(75, 23);
            this.btnAttaque1J2.TabIndex = 3;
            this.btnAttaque1J2.Text = "Attaque 1";
            this.btnAttaque1J2.UseVisualStyleBackColor = true;
            this.btnAttaque1J2.Click += new System.EventHandler(this.btnAttaque1J2_Click);
            // 
            // progressBarMonstre2
            // 
            this.progressBarMonstre2.Location = new System.Drawing.Point(6, 48);
            this.progressBarMonstre2.Name = "progressBarMonstre2";
            this.progressBarMonstre2.Size = new System.Drawing.Size(300, 23);
            this.progressBarMonstre2.TabIndex = 2;
            // 
            // picMonstre2
            // 
            this.picMonstre2.Location = new System.Drawing.Point(6, 48);
            this.picMonstre2.Name = "picMonstre2";
            this.picMonstre2.Size = new System.Drawing.Size(300, 400);
            this.picMonstre2.TabIndex = 1;
            this.picMonstre2.TabStop = false;
            // 
            // btnChoisirJ2
            // 
            this.btnChoisirJ2.Location = new System.Drawing.Point(6, 19);
            this.btnChoisirJ2.Name = "btnChoisirJ2";
            this.btnChoisirJ2.Size = new System.Drawing.Size(75, 23);
            this.btnChoisirJ2.TabIndex = 0;
            this.btnChoisirJ2.Text = "Choisir";
            this.btnChoisirJ2.UseVisualStyleBackColor = true;
            this.btnChoisirJ2.Click += new System.EventHandler(this.btnChoisirJ2_Click);
            // 
            // FrmCombat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 483);
            this.Controls.Add(this.grpJoueur2);
            this.Controls.Add(this.grpJoueur1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCombat";
            this.Text = "Combat";
            this.Load += new System.EventHandler(this.FrmCombat_Load);
            this.grpJoueur1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMonstre1)).EndInit();
            this.grpJoueur2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMonstre2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnChoisirJ1;
        private System.Windows.Forms.GroupBox grpJoueur1;
        private System.Windows.Forms.ProgressBar progressBarMonstre1;
        private System.Windows.Forms.PictureBox picMonstre1;
        private System.Windows.Forms.Button btnAttaque2J1;
        private System.Windows.Forms.Button btnAttaque1J1;
        private System.Windows.Forms.GroupBox grpJoueur2;
        private System.Windows.Forms.Button btnAttaque2J2;
        private System.Windows.Forms.Button btnAttaque1J2;
        private System.Windows.Forms.ProgressBar progressBarMonstre2;
        private System.Windows.Forms.PictureBox picMonstre2;
        private System.Windows.Forms.Button btnChoisirJ2;
    }
}

